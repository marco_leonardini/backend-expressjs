const { gitDescribeSync } = require('git-describe');
const { resolve, relative } = require('path');
const { writeFileSync } = require('fs-extra');

const gitInfo = gitDescribeSync({
    dirtyMark: false,
    dirtySemver: false
});

const file = resolve( __dirname, '..', 'config', 'version.json');

writeFileSync(file, JSON.stringify(gitInfo, null, 4), { encoding: 'utf-8' });

console.log(`Wrote version info ${gitInfo.raw}`);
