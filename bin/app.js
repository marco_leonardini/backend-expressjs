const express = require('express');
const app = express();
const bodyParser = require('body-parser');


const port = process.env.PORT || 8080;        // set our port

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

console.log('Environment: ', process.env.APP_ENV);
/** 
 * Load deafult routing
 */
require('../routing/app.routing')(app);

// Inicia el server.
app.listen(port, () => console.log(`Backend app listening on port ${port}! `));


module.exports = app;