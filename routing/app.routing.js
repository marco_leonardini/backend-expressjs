const version = require('../config/version.json');

module.exports = (app) => {

    /*
     * Adiciona el header CORS a todas las solicitudes.
     */
    app.all("/*", (req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        next();
    });

    /*
     * Adiciona headers CORS para las solicitudes OPTIONS.
     */
    app.options("/*", (req, res, next) => {
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Content-Length');
        res.header('Access-Control-Max-Age', 6000);
        res.send(200);
    });

    /*
     * Agrega ruta inicial para redireccionar a /api.
     */
    app.get('/', (req, res) => res.redirect('/api'));

    /**
   * 
   * @api {get} /api/ Información de la API
   * @apiName Información de la API
   * @apiGroup Auth
   * @apiVersion 1.0.0            
   * 
   * @apiSuccess {String}  message  Mensaje de bievenida.
   * @apiSuccess {String}  version  Version del sistema.
   * @apiSuccess {String}  env  Environment de ejecución.
   * 
   * @apiSuccessExample {json} Success-Response:
     {
         "message" : "Bienvenido al backend API del sistema BASE",
         "version" : "1.0.0",
         "env" : "dev"        
     }
   */
    app.get('/api', (req, res) => {
        const env = process.env.APP_ENV;
        const versionRaw = (env == 'prod') ? version.semver.raw : version.raw;

        res.status(200).send({
            message: `Bienvenido al backend API del sistema`,
            version: versionRaw,
            env: env
        });
    });
}
